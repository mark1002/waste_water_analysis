from django.apps import AppConfig


class IvsServiceConfig(AppConfig):
    name = 'ivs_service'
